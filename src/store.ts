import axios from "axios";
import * as LRU from "lru-cache";

type Headers = any;

const store = new LRU<string, Headers>({ max: 10000 });

export async function lookup(url: string): Promise<Headers> {
  if (!store.has(url)) {
    const response = await axios.head(url);
    store.set(url, response.headers);
  }

  return store.get(url);
}
