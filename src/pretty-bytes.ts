export enum BYTE_EXPONENT {
  B = 0,
  KiB,
  MiB,
  GiB,
  TiB
}

export default class PrettyBytes {
  exponent: BYTE_EXPONENT;
  figs: string;

  constructor(private raw: number, precision: number = 3) {
    let exponent = 0;
    while (raw > 1024) {
      raw /= 1024;
      exponent++;
    }
    this.figs = raw.toPrecision(precision);
    this.exponent = exponent;
  }

  toString() {
    return this.figs + " " + BYTE_EXPONENT[this.exponent];
  }
}
