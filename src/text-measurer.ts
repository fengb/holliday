import * as PDFDocument from "pdfkit";

export class PDFKitTextMeasurer {
  private document: PDFKit.PDFDocument;

  constructor(...fontPaths: string[]) {
    this.document = new PDFDocument({
      size: "A4",
      layout: "landscape"
    }).fontSize(11);
    for (const fontPath of fontPaths) {
      try {
        this.document.font(fontPath);
        return;
      } catch (e) {
        console.warn(
          `Text-width computation may be incorrect. Unable to load font at ${fontPath}.`
        );
      }
    }
    throw new Error("Could not load any fonts");
  }

  widthOf(str: string): number {
    return this.document.widthOfString(str);
  }
}

export class QuickTextMeasurer {
  private baseMeasurer: PDFKitTextMeasurer;
  private charCache = {} as { [index: string]: number };
  private kerningCache = {} as { [index: string]: number };

  constructor(...fontPaths: string[]) {
    this.baseMeasurer = new PDFKitTextMeasurer(...fontPaths);
  }

  _widthOfChar(char: string) {
    if (!this.charCache[char]) {
      this.charCache[char] = this.baseMeasurer.widthOf(char);
    }
    return this.charCache[char];
  }

  _widthOfKerning(first: string, second: string) {
    if (second === null) {
      return 0;
    }
    const pair = first + second;
    if (!this.kerningCache[pair]) {
      this.kerningCache[pair] =
        this.baseMeasurer.widthOf(pair) -
        this._widthOfChar(first) -
        this._widthOfChar(second);
    }
    return this.kerningCache[pair];
  }

  widthOf(str: string) {
    let result = 0;
    let prev = null;
    for (const char of str) {
      result += this._widthOfChar(char) + this._widthOfKerning(char, prev);
      prev = char;
    }
    return result;
  }
}

export default QuickTextMeasurer;
