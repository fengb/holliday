import TextMeasurer from "./text-measurer";

const measurer = new TextMeasurer("DejaVuSans.ttf", "Helvetica");

export interface BadgeSegment {
  text: string
  fillColor: string
}

export default function badge(left: BadgeSegment, right: BadgeSegment): string {
  const leftWidth = Math.ceil(measurer.widthOf(left.text)) + 10;
  const rightWidth = Math.ceil(measurer.widthOf(right.text)) + 10;

  return `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="${leftWidth+rightWidth}" height="20">
  <style>
    text {
      fill: #fff;
      font-size: 11px;
      font-family: DejaVu Sans, Verdana, Geneva, sans-serif;
      text-shadow: 0 1px rgba(1, 1, 1, 0.3);
      text-anchor: middle;
      length-adjust: spacing;
    }
  </style>
  <linearGradient id="smooth" x2="0" y2="100%">
    <stop offset="0" stop-color="#bbb" stop-opacity=".1"/>
    <stop offset="1" stop-opacity=".1"/>
  </linearGradient>

  <clipPath id="round">
    <rect width="${leftWidth + rightWidth}" height="20" rx="3" fill="#fff"/>
  </clipPath>

  <g clip-path="url(#round)">
    <rect width="${leftWidth}" height="20" fill="${left.fillColor}"/>
    <rect x="${leftWidth}" width="${rightWidth}" height="20" fill="${right.fillColor}"/>
    <rect width="${leftWidth + rightWidth}" height="20" fill="url(#smooth)"/>
  </g>

  <g>
    <text y="14" x="${leftWidth/2 + 1}" textLength="${leftWidth - 10}">${left.text}</text>
    <text y="14" x="${leftWidth + rightWidth/2 - 1}" textLength="${rightWidth - 10}">${right.text}</text>
  </g>
</svg>`
}
