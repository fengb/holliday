import * as Koa from "koa";

import { lookup } from "./store";
import badge, { BadgeSegment } from "./badge";
import PrettyBytes, { BYTE_EXPONENT } from "./pretty-bytes";

function sizeSegment(length: number | undefined): BadgeSegment {
  const COLORS: { [K in BYTE_EXPONENT]: string } = {
    [BYTE_EXPONENT.B]: "brightgreen",
    [BYTE_EXPONENT.KiB]: "green",
    [BYTE_EXPONENT.MiB]: "yellowgreen",
    [BYTE_EXPONENT.GiB]: "yellow",
    [BYTE_EXPONENT.TiB]: "orange"
  };

  if (length === undefined) {
    return { text: "unknown", fillColor: "lightgrey" };
  }

  const bytes = new PrettyBytes(length);
  return {
    text: bytes.toString(),
    fillColor: COLORS[bytes.exponent] || "red"
  };
}

const app = new Koa().use(async (ctx, next) => {
  const headers = await lookup(`http://${ctx.path.substr(1)}${ctx.search}`);
  const length = headers["content-length"];
  ctx.set("Content-Type", "image/svg+xml");
  ctx.body = badge(
    { text: "download", fillColor: "#555" },
    sizeSegment(headers["content-length"])
  );
});

export default app;
