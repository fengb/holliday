import config from "./src/config";
import server from "./src/server";

server.listen(config.port, () => {
  console.log(`Started on port ${config.port}`);
});
