FROM node:alpine

RUN mkdir /app
WORKDIR /app

COPY ./package.json /app
RUN yarn install

COPY . /app
RUN yarn tsc

RUN yarn install --production
RUN find . -name "*.ts" -exec rm {} \;

EXPOSE 3000
CMD ["node", "."]
